﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Key {

    public const byte W = 1;
    public const byte S = 2;
    public const byte A = 4;
    public const byte D = 8;
}
