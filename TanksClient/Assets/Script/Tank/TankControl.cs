﻿using RUCP;
using UnityEngine;

public class TankControl : MonoBehaviour {

    private byte keyState = 0;//DASW<0
    private byte lastState = 1;


    private void OnGUI()
    {
        if (Event.current.isKey)
        {
            Event _event = Event.current;
            switch (_event.keyCode)
            {
                case KeyCode.W:
                    if(IsDifference(Key.W, _event.type))
                    {
                        SetKeyState(Key.W, _event.type);
                        SendState();
                    }
                    break;
                case KeyCode.S:
                    if (IsDifference(Key.S, _event.type))
                    {
                        SetKeyState(Key.S, _event.type);
                        SendState();
                    }
                    break;
                case KeyCode.A:
                    if (IsDifference(Key.A, _event.type))
                    {
                        SetKeyState(Key.A, _event.type);
                        SendState();
                    }
                    break;
                case KeyCode.D:
                    if (IsDifference(Key.D, _event.type))
                    {
                        SetKeyState(Key.D, _event.type);
                        SendState();
                    }
                    break;
            }
        }
    }

   
    private bool IsDifference(byte code, EventType type)
    {
             //Если клавиша нажата    
        if (type == EventType.KeyDown)
        {
            // и  последнее состояние клавиши "нажата"    
            if ((code & keyState) != 0) return false;
        }
        else  //Если клавиша не нажата    
        {
            // и  последнее состояние клавиши "не нажата"    
            if ((code & keyState) == 0) return false;
        }
        return true;
    }

    private void SetKeyState(byte code, EventType type)                                     
    {
        if (type == EventType.KeyDown)
        {
            keyState |= code;
        }
        else
        {
            keyState ^= code;
        }

    }

    private void SendState()
    {
        NetworkWriter nw = new NetworkWriter(Channels.Reliable, 2);
        nw.SetTypePack(Types.KeyControl);
        byte[] data = new byte[2];
        data[0] = lastState;
        data[1] = keyState;
        nw.write(data);
        NetworkManager.Send(nw);

        lastState++;
        lastState %= 101;  
    }
}
