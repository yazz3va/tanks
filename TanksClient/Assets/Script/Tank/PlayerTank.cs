﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTank : MonoBehaviour, GhostTank {
    public float smooth = 0.03f;
    private string name_tank;
    public int ID;
    private const float radian = 180.0f / 3.14159265359f;

    public void SetStartPosition(float x, float y)
    {
        rigid.position = new Vector3(x, y, 0.0f);
        next_position = rigid.position;
    }
    public void SetName(string _name)
    {
        name_tank = _name;
    }



    private Rigidbody2D rigid;
    // private Animator anim;
    private Vector3 next_position;
    private bool walk_anim = false;

    private float e_angle;

    private void Start()
    {
        Initial();
    }


    private void FixedUpdate()
    {
        if (Vector3.Distance(rigid.position, next_position) > 0.01f)
        {
            //  if (!walk_anim) { anim.SetBool("walk", true); walk_anim = true; }
            rigid.position = Vector3.MoveTowards(rigid.position, next_position, smooth);

        }
        else
        {
            //  if (walk_anim) { anim.SetBool("walk", false); walk_anim = false; }
        }

        rigid.rotation = Mathf.LerpAngle(rigid.rotation, e_angle, smooth);
    }

    public void Initial()
    {
        if (rigid != null) return;
        rigid = GetComponent<Rigidbody2D>();
        //  anim = GetComponent<Animator>();

      
    }



    public void Move(float x, float y, float rotation)
    {
        next_position.x = x;
        next_position.y = y;

        e_angle = rotation * radian;
    }
}
