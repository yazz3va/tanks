﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Types {

    public const short CloseConnection = 1;// S->S Метод выполняемы во время отключения игрока 
    public const short LobbyEntrance = 2; //c->s,s->c Вход игрока в общую комнату
    public const short MyCharacters = 3; //C->S запрос на получение списка персонажей, S->C Список персонажей
    public const short OwnCharacterCreate = 4; //C->S->C Отправка на сервер запроса на создание персонажа и отпрака обратно подтверждение или ошибки
    public const short SetCharacter = 5; //S->C Выбор персонажа
    public const short MyCCharacter = 6; //C->S Запрос на получение информации о выбраном персонаже S->C Информация о выбраном персонаже
    public const short RoomFind = 7; // S->C Запрос на поиск комнаты 
    public const short Login = 8;  //C->S отпровляет логин и пароль на сервер
    public const short Registration = 9; //C->S отпровляет логин и пароль  для регистрации акаунта
    public const short LoginInfo = 10; //S->C  Отпровляет код ошибки или успешного выполнение запроса
    public const short LoginOk = 11; //S->C Логин успешен
    public const short ServersList = 12; //C->S запрос на получение списка серверов S->C Список серверов
    public const short Move = 13;  //C->S Передвижение персонажа на карте
    public const short Charactermove = 14; //S->C Передвижение других игроков
    public const short RoomEntrance = 15; //С->S На клиенте загрузилась комната, запрос на получение данных
    public const short CharacterCreate = 16; // S->C данные для создание других игроков
    public const short KeyControl = 17;//C->S Запрос на взятие цели по ид. S->C ответ о возможности взять в цель
}
