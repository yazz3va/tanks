﻿using RUCP;
using RUCP.Handler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lobby : MonoBehaviour {

    public Text text_info;
    private bool order = false;
	
	void Start () {
        ConnectionLobby();
        RegisteredTypes.RegisterTypes(Types.RoomFind, RoomFindAnswer);
    }


    //1- Успешное подключение
    //2- Персонаж не выбран
    //3- Комната переполнена повторите попытку
    private void RoomFindAnswer(NetworkWriter nw)
    {
        byte d = nw.ReadByte();
        print("Room answer: "+ d);
        switch (d)
        {
            case 1:
                SceneManager.LoadScene("Map");
                break;
            case 2:
                text_info.text = "Персонаж не выбран";
                break;
            case 3:
                text_info.text = "Комната переполнена повторите попытку";
                break;
            default:
                text_info.text = "Неизвестная ошибка";
                break;
        }
        order = false;
    }

    private  void ConnectionLobby()
    {
        NetworkWriter nw = new NetworkWriter(Channels.Reliable, 0);
        nw.SetTypePack(Types.LobbyEntrance);
        NetworkManager.Send(nw);
    }

    public void ConnectionRoom()
    {
        if (order) return;
        order = true;
        NetworkWriter nw = new NetworkWriter(Channels.Reliable, 0);
        nw.SetTypePack(Types.RoomFind);
        NetworkManager.Send(nw);
    }
    private void OnDestroy()
    {
        RegisteredTypes.UnregisterTypes(Types.RoomFind);
    }
}
