﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraZoom : MonoBehaviour {

    public Camera cyr_cam;
    public int zoomFactor = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        cyr_cam.orthographicSize-=Input.GetAxis("Mouse ScrollWheel")*zoomFactor;
    }
}
