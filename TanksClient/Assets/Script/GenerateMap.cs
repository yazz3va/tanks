﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMap : MonoBehaviour {
    [SerializeField] private float x;
    [SerializeField] private float y;
    [SerializeField] private float w;
    [SerializeField] private float h;
    [SerializeField] private int rows;
    [SerializeField] private int cols;
    [SerializeField] private float p;
    [SerializeField] private float house_size_x_from;
    [SerializeField] private float house_size_x_to;
    [SerializeField] private float house_size_y_from;
    [SerializeField] private float house_size_y_to;
    [SerializeField] private GameObject trees;

    // Use this for initialization
    void Start()
    {
    }

    private void Awake()
    {
        Generate_map(x, y, w, h, rows, cols, p);
    }

    private void Generate_map(float x, float y, float w, float h, int rows, int cols, float p)
    {
        float xd = w / cols;
        for (int ix = 0; ix < (cols + 1); ix++)
        {
            StartCoroutine(Generate(xd, ix, p, house_size_x_from, house_size_x_to, house_size_y_from, house_size_y_to));
        }
    }


    private IEnumerator Generate(float xd, float ix, float p, float house_size_x_from, float house_size_x_to, float house_size_y_from, float house_size_y_to)
    {
        float yd = h / rows;
        for (int iy = 0; iy < (rows + 1); iy++)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Quad);
            DestroyImmediate(cube.GetComponent<MeshCollider>());
            GameObject tree = GameObject.Instantiate(trees);
            cube.transform.parent = transform;
            tree.transform.parent = transform;
            cube.transform.position = new Vector3(x + xd * ix + Random.Range(-p, p) * xd, y + yd * iy + Random.Range(-p, p) * yd, 1);
            tree.transform.position = new Vector3(x + xd * ix + Random.Range(-p, p) * xd, y + yd * iy + Random.Range(-p, p) * yd, 2);
            cube.transform.localScale = new Vector3(Random.Range(house_size_x_from, house_size_x_to), Random.Range(house_size_y_from, house_size_y_to), 1);
            cube.AddComponent<BoxCollider2D>();
        }

        yield return 1;
    }


    // Update is called once per frame
    void Update()
    {

    }
}
