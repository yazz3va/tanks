﻿using RUCP.Handler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RUCP;
using System;
using UnityEngine.UI;
using System.Text;

public class SL_Dropdown : MonoBehaviour {

    private Dropdown dropdown;
    private List<string> ip;
    private List<int> port;
	
	void Start () {
        ip = new List<string>();
        port = new List<int>();
        dropdown = GetComponent<Dropdown>();
        RegisteredTypes.RegisterTypes(Types.ServersList, ServersList);
        GetServersList();
    }


    private void GetServersList()
    {
        // print("Get list");
        NetworkWriter nw = new NetworkWriter(Channels.Reliable, 0);
        nw.SetTypePack(Types.ServersList);
        NetworkManager.Send(nw);
    }


    private void ServersList(NetworkWriter nw)
    {
      //  print(nw.AvailableBytes);
        List<string> names = new List<string>(10);
        while (nw.AvailableBytes > 0)
        {
            //  print("create");
          
          
            names.Add(Encoding.ASCII.GetString(nw.read(nw.ReadByte())));
            ip.Add(Encoding.ASCII.GetString(nw.read(nw.ReadByte())));
            port.Add(nw.ReadInt());

        }
       // foreach (string n in names) print(n);
        dropdown.ClearOptions();
        dropdown.AddOptions(names);
 
        if(ip.Count > 0) SL_Main.Instance.SetAdress(ip[0], port[0]);

    }

    public void SetServer(int i)
    {
        SL_Main.Instance.SetAdress(ip[dropdown.value], port[dropdown.value]);
    }

    private void OnDestroy()
    {
        RegisteredTypes.UnregisterTypes(Types.ServersList);
    }
}
