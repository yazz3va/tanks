﻿
using RUCP;
using RUCP.Handler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {

  



    void Start()
    {
        RegisteredTypes.RegisterTypes(Types.LoginInfo, LoginInfo);
        RegisteredTypes.RegisterTypes(Types.LoginOk, LoginOk);
    }

    private void LoginOk(NetworkWriter nw)
    {
        NetworkManager.SetLoginID(nw.ReadInt());
        NetworkManager.SetKey(nw.ReadInt());
        SceneManager.LoadScene("ServersList");
    }

    private void LoginInfo(NetworkWriter nw)
    {
        byte code = nw.ReadByte();

        switch (code)
        {
            case 1: //Такой логин уже используется
                LoginInformation.ShowInfo(4);
                break;
            case 2://Логин не должен содержать меньше 3 или больше 30 символов
                LoginInformation.ShowInfo(2);
                break;
            case 3://пароль не должен содержать меньше 3 или больше 30 символов
                LoginInformation.ShowInfo(3);
                break;
            case 4://Регистрация успешна
                LoginInformation.ShowInfo(5);
                break;
            case 5://Версия клиента не соответствует серверу
                LoginInformation.ShowInfo(6);
                break;
            case 6://Логин или пароль неверны
                LoginInformation.ShowInfo(7);
                break;
            case 7://Версия клиента устарела
                LoginInformation.ShowInfo(8);
                break;
        }
    }


    private void OnDestroy()
    {

        RegisteredTypes.UnregisterTypes(Types.LoginInfo);
        RegisteredTypes.UnregisterTypes(Types.LoginOk);
    }
}
