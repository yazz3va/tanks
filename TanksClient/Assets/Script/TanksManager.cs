﻿using RUCP.Handler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RUCP;
using System;
using System.Text;

public class TanksManager : MonoBehaviour {

    public GameObject tankObject;
    private Dictionary<int, GhostTank> tanks;
	
	void Start () {
        tanks = new Dictionary<int, GhostTank>(20);

        RegisteredTypes.RegisterTypes(Types.CharacterCreate, TankCreate);
        RegisteredTypes.RegisterTypes(Types.Charactermove, Tankmove);

        RoomEntrance();
    }

    public void RoomEntrance()
    {
        NetworkWriter nw = new NetworkWriter(Channels.Reliable, 0);
        nw.SetTypePack(Types.RoomEntrance);
        NetworkManager.Send(nw);
    }

    private void TankCreate(NetworkWriter nw)
    {
        int idPlayer = nw.ReadInt();
        int idTank = nw.ReadInt();

        print("Tankkk: " + idPlayer);

        if (!tanks.ContainsKey(idPlayer))
        {
            GhostTank ghost = null;
            if (NetworkManager.GetLoginId() == idPlayer)
            {
                ghost = GameObject.Find("TankPlayer").GetComponent<PlayerTank>();
                ghost.Initial();
                ghost.SetStartPosition(nw.ReadFloat(), nw.ReadFloat());
            }
            else
            {
                GameObject obj = Instantiate(tankObject, Vector3.zero, Quaternion.identity);
                EnemyTank tank_obj = obj.GetComponent<EnemyTank>();
                tank_obj.Initial();
                tank_obj.SetStartPosition(nw.ReadFloat(), nw.ReadFloat());
                tank_obj.SetName(Encoding.ASCII.GetString(nw.read(nw.ReadByte())));
                tank_obj.ID = idPlayer;
                ghost = tank_obj;
            }
           
            tanks.Add(idPlayer, ghost);
        }
        else
        {
            print(idPlayer + " персонаж уже был загружен");
        }
    }

    private void Tankmove(NetworkWriter nw)
    {
        int idChar = nw.ReadInt();
  
        if (tanks.ContainsKey(idChar))
        {
            tanks[idChar].Move(nw.ReadFloat(), nw.ReadFloat(), nw.ReadFloat());
          //  print(idChar + " : " + nw.GetNumberPack());
        }
        else
        {
            print("Tank not found: " + idChar);
        }
    }

    private void OnDestroy()
    {
        RegisteredTypes.UnregisterTypes(Types.CharacterCreate);
        RegisteredTypes.UnregisterTypes(Types.Charactermove);
    }
}
